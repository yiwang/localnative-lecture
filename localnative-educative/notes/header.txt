Architecture
凡事预则立，不预则废。《礼记·中庸》


Project structure
不以规矩，不能成方圆。《孟子·离娄上》



rustup
工欲善其事，必先利其器。《论语·卫灵公》



Rust Library Entry Point
千里之行，始于足下。《老子·道德经》



Build Script
不以规矩，不能成方圆。《孟子·离娄上》


Sister project Fastxt
旧瓶装新酒