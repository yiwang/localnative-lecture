# Develop Decentralized Cross-platform Apps - Software Engineering Exercise 2020-2021

## Goal
- Setup the tool chain to build the [source](https://gitlab.com/localnative/localnative) [code](https://gitlab.com/fastxt/fastxt) and run [Local Native](https://localnative.app) and [Fastxt](https://fastxt.app), which includes [Desktop application, Web browser extension](https://youtu.be/DBsVscpSp6w), [iOS](https://youtu.be/3dhB5gTtXNM), [iPadOS](https://youtu.be/tZhbwV_a0fs)  and [Android](https://play.google.com/store/apps/details?id=app.localnative).
- Discover the story and people behind those tools.
- Explore various software engineering concepts.

## Non-goal
- In-depth coverage of any particular language, tool or algorithm.
- It's encouraged to hack the code to experiment adding new features, but not required.

## Prerequisite
- Able to setup and trouble shot software installation process.
- Able to work with (and enjoy!) typing in non-GUI command-line interface.
- Able to fluently switch between multiple applications and track file locations.
- Some programming experience is preferred.

## Hardware and Operating System Requirement
- Hardware
    - CPU: 4-core or above
    - Memory: 8 GB RAM or above
    - Free disk space: 50 GB or above
- Operating System
    - Apple Computer with new macOS Catalina OS, or
    - Ubuntu
        - Native Installation on Hardware
            - Dual Boot (if you have a Windows machine)
            - Able to use Android Device Simulator without physical device
        - Installed as VirtualBox guest OS
            - Not able to use Android Device Simulator, need physical Android device
- Mobile Device
    - Apple iOS / iPadOS version 13 or above
    - Android mobile device 
  
## Operating System Notes
- Only macOS supports iOS / iPadOS development.

|OS           | Browser<br>Extension | Desktop<br>Application| iOS / iPadOS   | Android   |
|-------------|----|----|----|----|
|Ubuntu       | ✓  | ✓  |    | ✓  |
|Mac          | ✓  | ✓  | ✓  | ✓  |


# 开发去中心跨平台应用 - 软件工程实践 2020-2021

## 目标
- 安装配置一系列开发工具，使能运行 [Local Native](https://localnative.app) 和 [Fastxt](https://fastxt.app) 的[代](https://gitlab.com/localnative/localnative)[码](https://gitlab.com/fastxt/fastxt)，涉及[桌面程序，浏览器扩展](https://youtu.be/DBsVscpSp6w)，[iOS](https://youtu.be/3dhB5gTtXNM)， [iPadOS](https://youtu.be/tZhbwV_a0fs) 和 [安卓](https://play.google.com/store/apps/details?id=app.localnative)开发。
- 发现这些工具背后的故事和人。
- 探索一些软件工程的概念。

## 非目标
- 不会深纠某一个具体的语言，工具和算法。
- 鼓励 hack 代码增加新特性，但不是必须。

## 基础
- 能安装软件并解决安装过程中的问题。
- 能（并喜欢！）在非图形的命令行界面中工作。
- 能熟练地在不同的软件之间切换，能准确定位各种文件的位置。
- 推荐有一定的编程经验。

## 硬件和操作系统要求
- 硬件
    - CPU: 4 核以上
    - 内存: 8 GB 以上
    - 磁盘空间: 50GB 以上空闲
- 操作系统
    - 苹果电脑 和 最新的 macOS Catalina 操作系统, 或者
    - Ubuntu
        - 原生安装在硬件
            - 双启动 (如果原来是 Windows 系统)
            - 可以使用 Android 模拟器模拟移动设备
        - 安装在 VirtualBox 虚拟机内部
            - 不能使用 Android 模拟器, 需要有物理 Android 移动设备
- 移动设备
    - 苹果 iOS / iPadOS 版本 13 以上
    - 安卓 Android 移动设备

## 操作系统备注
- 只有 Mac 支持 iOS / iPadOS 开发。

|OS           | Browser<br>Extension | Desktop<br>Application| iOS / iPadOS   | Android   |
|-------------|----|----|----|----|
|Ubuntu       | ✓  | ✓  |    | ✓  |
|Mac          | ✓  | ✓  | ✓  | ✓  |
