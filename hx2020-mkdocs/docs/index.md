# Develop Decentralized Cross-platform Apps - Software Engineering Exercise 2020-2021

## Spring week 15
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/c-HSCWN2C7A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- WeChat official account backend UI review
- Fastxt code review
    - druid UI
- Functional programming
    - val vs var
    - lens concept  


## Spring week 14
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/-kaPdcMs-bM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
- Static website generators
    - docusaurus
    - zola
    - mkdocs
    - vuepress

## Spring week 13
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/RR5sQOgpd3I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Time Tracking
    - gtt
- GitLab CI/CD  
    - Docker
- xshell: Rust scripting
- Visualization
    - d3.js, dc.js

## Spring week 12
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/RiydPooSmCc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- VSCode with rust-analyzer
- code review `localnative_iced` main function
- Rust GUI graphics driver  
- Async overview

## Spring week 11
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/3MAL51Bwou0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Code review
    - iced example color_palette and color space
      
- Overview of Rust bridge for Android and iOS  
    - crate-type
    - declarative UI and event handler pattern
    - device emulator issues

## Spring week 10
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2KF3_AMJK90" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Cargo
    - manage package, run build and test
- Git GUI
    - gitk, gitg
- Code review  
    - `localnative_bundle`

## Spring week 9
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/SHtvBF1NlIE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Code review
    - linked_hash_set
    - localnative_iced PR overview

## Spring week 8
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/iULf1doWGvU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- `localnative_iced` progress
- Git branching management
    - Gitflow
    - AoneFlow
- Code review
    - `qrcode-rust`
    - druid `view_switcher`, `widget_gallery` example

## Spring week 7
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/BjKEff0U91c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- `git remote add upstream `
- demo `runebender` 
- Code review
    - iced `tour` example

## Spring week 6
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/hjfvoiPIBuk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Code review
    - iced `todos` example
    - druid `game_of_life` example

## Spring week 5
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Y2sJwjUgE24" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- TodoMVC 
- Code review of iced `qr_code` example
    - Rust module, struct and trait

## Spring week 4
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/8sPdRUoI_wE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- git branch
    - tig, gitk
    - merge request


## Spring week 3
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/LCA3ghhmrko" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Spike
    - druid layout
    - Windows registry
- SMART goals framework

## Spring week 2
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/psMD6omfb4U" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- OKR, Agile framework
- Spike QR code in druid and iced

## Spring week 1
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/GWyzhpDg8Ck" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Roles in software project
- Software license  
    - AGPL
- Rust GUI frameworks
    - druid
    - iced
        - qr code

## Fall week 15
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Epg28AVai8o" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Fall review
- GitLab issues

## Fall week 14
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/mL7A1LgS-8c" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Electron
    - share V8 JavaScript Engine with Chrome
    - npm entry point
    - npm run start/dev
- review n
    - N_PREFIX

## Fall week 13
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/K5E7N3KBB74" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- git
    - gitk
    - tig
    - merge vs rebase
- build `localnative-electron` release artifact
- n   
  
## Fall week 12
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/M88o-8VJzxU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- shell script $ variables 
- regex search and replace
- lock file `package-lock.json` and `Cargo.lock`
- `script/release-appimage`
    - `ln -s /bin/sed gsed`
    - [n](https://github.com/tj/n)
    - `npm i -g electron-builder`

## Fall week 11
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/juPV0MqfoS4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Code review of browser extension
    - browser extension `manifest.json`
    - `popup.html` and `popup.js`  
    - app manifest `app.localnative.json` setup
- Software license
- md5 code checksum

## Fall week 10
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Iddo898v87c" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Browser Extension Components & Data Flow 
    - DOM (HTML, CSS, JavaScript)
    - Browser API
- .idea folder
- Markdown

## Fall week 9
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Isbyq1FIlIQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Introduction to Local Native desktop application
    - Electron
    - npm
    - Node.js
    - Neon 
- App manifest of browser extension
    - Location of native messaging app manifest file
    - Browser security

## Fall week 8
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/C2EJlh2jNmY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Communication between web extension and localnative-web-ext-host binary - stdin, stdout 
- Review environment variable PATH
    - echo $PATH
    - export PATH in ~/.bashrc
    - env
    - source ~/.bashrc
- Review IDEA Rust plugin to build Cargo targets
    - rustup
    - rustc
    - cargo clean, cargo build

## Fall week 7
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/DzPXNl01LNM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- `sqlite3` attach another database
    - Concept of consistency in [ACID](https://en.wikipedia.org/wiki/ACID)
- Local Native table creation
    - SQLite table constraints
        - not null, unique, primary key, autoincrement
- Linus Torvalds [quotes on data structures](https://en.wikiquote.org/wiki/Linus_Torvalds#:~:text=Bad%20programmers%20worry%20about%20the,data%20structures%20and%20their%20relationships.)

## Fall week 6
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/rSjFuHo0dcA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- SQLite
    - `sqlite3` command line tool
    - DB Browser for SQLite
- Concept of stdin, stdout
    - pipe `|`
    - output redirection `>`
- Local Native demo syncing from iOS to Mac Desktop

## Fall week 5
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/DZ_ORh2YTQw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Local Native browser extension and desktop application demo
- Local Native Rust core library entry point and crate type
- Local Native build script
    - Atom editor
    - `script/set-version` example
        - Symbolic link: gsed example
        - Git GUI: tig, gitk, gitg

## Fall week 4
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/vDDhFWO2QZM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- git clone localnative repo
- Setup rustup and cargo
- Setup IDEA with Rust plugin
- Environment variables and $PATH

## Fall week 3
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/1wybi0B8x3A" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Setup dotfiles
    - git clone use https protocol
- Install Toolbox
    - IDEA Community
    - Android Studio
    - PyCharm Community
- Install Atom

## Fall week 2
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Yb2rT9IQ2bw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Install Ubuntu 20.04 LTS in VirtualBox 6.1
    - Unchecked `Enable Audio`
- Installed Docker Desktop

## Fall week 1
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/a-qGXmoRtbw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Introduction to Local Native Apps
- Local Native high level [diagrams](https://hx2019.localnative.app/diagram/diagram-3.html)
- Ubuntu (Dual Boot preferred for student not using Mac) - [video](https://www.youtube.com/watch?v=u5QyjHIYwTQ&t=137s)
