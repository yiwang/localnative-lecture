<script src="https://unpkg.com/mermaid@8.8.4/dist/mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>


# localnative Component Diagrams

## Browser Extension Components & Data Flow

<div class="mermaid">
graph LR
    subgraph web browser
        subgraph browser web page tab
            title[web page title]
            html[web page html content]
        end
        subgraph localnative-browser-extension
            subgraph JavaScript - popup.js etc
                api[call browser api]
                nmc[native messaging client]
            end
            ext-html[extension HTML UI - popup.html]
            subgraph manifest[extension manifest.json]
                ep[extension permissions - nativeMessaging etc]
                ht[extension default_popup - popup.html]
            end
        end
    end
    subgraph app manifest - app.localnative.json
        p[host permission - allowed_origins/allowed_extensions]
        loc[host binary location - path]
        name[name - app.localnative]
    end
    subgraph web extension host binary: localnative-web-ext-host
        stdin
        stdout
        run[database CRUD operations]
        stdin --> run --> stdout
    end
    title --> api
    html --> api
    stdout --> nmc --> stdin
    loc --> nmc
</div>

# localnative Sequence Diagrams - Data Flows
## Browser Extension

<div class="mermaid">
sequenceDiagram
    participant E as Browser Extension
    participant C as localnative-web-ext-host
    participant S as localnative.sqlite3
    Note over E,C: stdin
    E->>+C: Command/Request JSON
    Note over C,S: Rust to SQL
    C->>+S: CRUD on Database
    S-->>-C: Database Response
    Note over C,E: stdout
    C-->>-E: Response JSON
</div>

## Desktop App
<div class="mermaid">
sequenceDiagram
    participant E as localnative-electron
    participant N as localnative-neon
    participant C as localnative_core
    participant S as localnative.sqlite3
    Note over E,N: JavaScript
    E->>+N: Command/Request JSON
    Note over N,C: JavaScript to Rust Binding
    N->>+C: Command/Request
    Note over C,S: Rust to SQL
    C->>+S: CRUD on Database
    S-->>-C: Database Response
    C-->>-N: Response
    N-->>-E: Response JSON
</div>

## Mobile App

<div class="mermaid">
sequenceDiagram
    participant E as Android and iOS App
    participant C as localnative_core
    participant S as localnative.sqlite3
    Note over E,C: JVM/Swift to Rust Bridge
    E->>+C: Command/Request JSON
    Note over C,S: Rust to SQL
    C->>+S: CRUD on Database
    S-->>-C: Database Response
    C-->>-E: Response JSON
</div>