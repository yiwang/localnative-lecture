# fall week 4 - 09/29
- Local Native @ [Rust NYC](https://github.com/rust-nyc/meetups/issues/13) [2019 Sep Meetup](https://www.meetup.com/Rust-NYC/events/264849068/) Talk [Slide](https://rust-nyc-2019.localnative.app/)

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Qvhw8ace6KQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
