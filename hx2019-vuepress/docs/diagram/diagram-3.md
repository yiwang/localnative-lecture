# diagram 3

## browser extension
<mermaid>
sequenceDiagram
    participant E as Browser Extension
    participant C as localnative-web-ext-host
    participant S as localnative.sqlite3
    Note over E,C: stdin
    E->>+C: Command/Request JSON
    Note over C,S: Rust to SQL
    C->>+S: CRUD on Database
    S-->>-C: Database Response
    Note over C,E: stdout
    C-->>-E: Response JSON
</mermaid>

## desktop app
<mermaid>
sequenceDiagram
    participant E as localnative-electron
    participant N as localnative-neon
    participant C as localnative_core
    participant S as localnative.sqlite3
    Note over E,N: JavaScript
    E->>+N: Command/Request JSON
    Note over N,C: JavaScript to Rust Binding
    N->>+C: Command/Request
    Note over C,S: Rust to SQL
    C->>+S: CRUD on Database
    S-->>-C: Database Response
    C-->>-N: Response
    N-->>-E: Response JSON
</mermaid>

## mobile app
<mermaid>
sequenceDiagram
    participant E as Android and iOS App
    participant C as localnative_core
    participant S as localnative.sqlite3
    Note over E,C: JVM/Swift to Rust Bridge
    E->>+C: Command/Request JSON
    Note over C,S: Rust to SQL
    C->>+S: CRUD on Database
    S-->>-C: Database Response
    C-->>-E: Response JSON
</mermaid>
