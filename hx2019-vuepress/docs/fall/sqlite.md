# sqlite

[SQLite](https://sqlite.org/index.html)
[Public Domain](https://sqlite.org/copyright.html)

## DB Browser for SQLite
[https://sqlitebrowser.org/](https://sqlitebrowser.org/)

## sqlcipher
[https://github.com/sqlcipher/sqlcipher](https://github.com/sqlcipher/sqlcipher)

[https://www.zetetic.net/contributions](https://www.zetetic.net/contributions)

[http://www.harmonyagreements.org/guide.html](http://www.harmonyagreements.org/guide.html)
