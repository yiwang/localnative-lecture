# fall week 13 - 01/05

- Local Native message passing and sequence diagrams
- code review of Neon (JavaScript into native Rust binding) in various Local Native modules
- quick intro to Qt and GTK

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/rGZ-HTaL1JE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
