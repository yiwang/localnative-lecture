# Swift

[Graydon from Rust to Swift](https://news.ycombinator.com/item?id=13533701)

[Swift vs Rust](https://medium.com/@eonil/choosing-swift-vs-rust-237bcb45d97b)

Apple official documentation: [Swift](https://developer.apple.com/swift/)
