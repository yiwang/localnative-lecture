# ios

[Building and Deploying a Rust library on iOS](https://mozilla.github.io/firefox-browser-architecture/experiments/2017-09-06-rust-on-ios.html)

## cargo lipo
[Cargo subcommand to automatically create universal libraries for iOS.](https://github.com/TimNN/cargo-lipo) 
```
cd localnative_rs/localnative_core
cargo lipo --release
```

This will build
```
localnative-rs/target/universal/release/liblocalnative_core.a
```

## symbolical link
```
localnative-ios/liblocalnative_core.a
```
is symbolically linked to the above rust library.

## lib crate type
To build for iOS, `Cargo.toml` must use `staticlib`
```
[lib]
name = "localnative_core"
# ios works as below
crate-type = ["staticlib"]
```
