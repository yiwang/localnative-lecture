# Software Downloads

It will take lengthy time to download and install below softwares, especially over slow network.

Please download and install them in advance.

- [iTerm2](https://iterm2.com)
- [Brew](https://brew.sh)
```
brew install git tig tree vim tmux
```
- [Xcode](https://developer.apple.com/xcode/) (via Mac App Store)

- Rust
	- [rustup](https://rustup.rs)

- IDE
	- [Toolbox App](https://www.jetbrains.com/toolbox/app/)
		- [IntelliJ IDEA](https://www.jetbrains.com/idea/)
			- [IntelliJ Rust](https://intellij-rust.github.io)
		- [Android Studio](https://developer.android.com/studio)
	- [Atom](https://atom.io)

- Browsers:
	- [Firefox](https://www.mozilla.org/)
	- [Chromiumn](https://www.chromium.org/getting-involved/download-chromium)
	- [Brave](https://brave.com)

- [DB Browser for SQLite](https://sqlitebrowser.org/)
