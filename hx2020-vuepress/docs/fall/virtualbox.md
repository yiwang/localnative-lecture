# [VirtualBox](https://en.wikipedia.org/wiki/VirtualBox)
[download](https://www.virtualbox.org/wiki/Downloads)

[enable virtualization in BIOS](https://bce.berkeley.edu/enabling-virtualization-in-your-pc-bios.html)

## Guest Addition
in order to use full screen, adjust screen size for guest OS etc.
```
apt search linux-headers-...
sudo apt install linux-headers-...
```
linux-headers is needed to successfully instal guest addition.

![virtualbox-guest-addition](/img/virtualbox-guest-addition.png)

## more
[太阳计算机系统](https://zh.wikipedia.org/wiki/%E6%98%87%E9%99%BD%E9%9B%BB%E8%85%A6)
 was [bought](https://www.oracle.com/corporate/pressrelease/oracle-buys-sun-042009.html) be Oracle.
