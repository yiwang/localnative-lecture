# pureos

[download](https://pureos.net/download/)

add the downloaded iso file to VM storage Optical Drive

after installation inside VM guest
```
sudo apt update
sudo apt upgrade

sudo apt install nemo
sudo apt install terminator
```

install process will take a while
![pureos-8-install](/img/pureos-8-install.png)

## more
[rolling releases](https://puri.sm/posts/pureos-rolls-on-as-stable/)

[fix](https://fixubuntu.com/) [the](https://www.gnu.org/philosophy/ubuntu-spyware.en.html) [search](https://micahflee.com/2013/11/canonical-shouldnt-abuse-trademark-law-to-silence-critics-of-its-privacy-decisions/)

[ubuntu edge](https://www.indiegogo.com/projects/ubuntu-edge#/) vs [librem-5](https://puri.sm/products/librem-5/)
