# diagram 2

<mermaid>
graph TD
Rust((Rust))-->|rustup|cargo-->mb[mdbook]
Git{Git}-->tig
Git-->gitk
Git-->gc[git clone]
SSH{SSH}-->|ssh-keygen| GitLab-->gc
gc-->ln(localnative-lecture)-->mb-->sw
ln-->vuepress-->|mermaid|sw("(static) website")-->firebase
gc-->ln-doc(localnative-app-docusaurus)-->npm-->sw
js((JavaScript))-->Node.js-->|n|npm
html((HTML))-->sw
css((css))-->sw
md((Markdown))-->sw
dns{DNS}-->sw
</mermaid>
