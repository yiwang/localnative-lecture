module.exports = {
  title: 'hx2020.localnative.app',
  description: 'HXNYC 2020-2021 Class: Develop Cross-platform Apps - Software Engineering Exercise',
  plugins: [
    'vuepress-plugin-mermaidjs'
  ],
  themeConfig: {
    displayAllHeaders: true, // Default: false
    nav: [
      { text: 'Home', link: '/' },
      { text: '2020 Fall', link: '/fall/fall-week-1' },
      { text: '2021 Spring', link: '/spring/spring-week-1-no-class' },
      { text: 'Diagram', link: '/diagram/diagram-1' },
      { text: 'Downloads', link: '/fall/downloads' },
      { text: 'Videos', link: 'https://www.youtube.com/channel/UCO3qFIyK0eSmqvMknsslWRw'},
      { text: 'Local Native', link: 'https://localnative.app' },
      { text: 'Fastxt', link: 'https://fastxt.app' },
      { text: 'Other Year', link: 'https://localnative.app/docs/lecture' }
    ],
    sidebar: {
      '/diagram/':[
        {
          title: 'Diagram',   // required
          collapsable: false, // optional, defaults to true
          children: [
            'diagram-1',
            'diagram-2',
            'diagram-3'
          ]
        }
      ],
      '/fall/':[
        {
          title: '2020 Fall Lecture',   // required
          collapsable: false, // optional, defaults to true
          children: [
            'fall-week-1',
            'localnative_app',
            'gitlab',
            'gitter',
            'github',
            'downloads',

            'fall-week-2',
            'gnu_linux',
            'virtualbox',
            'pureos',
            'librem_one',

            'fall-week-3',
            'cli',
            'git',
            'dotfiles',

            'fall-week-4',
            'fall-week-5',
            'fall-week-6',

            'fall-week-7',
            'ssh',
            'rust',
            'mdbook',

            'fall-week-8',
            'markdown',
            'html',
            'css',

            'fall-week-9',
            'node.js',
            'javascript',

            'fall-week-10',

            'fall-week-11',
            'json',
            'sqlite',
            'sql',

            'fall-week-12',
            'web_extension',
            'desktop',
            'electron',

            'fall-week-13',
            'neon',
            'qt',
            'gtk',

            'fall-week-14',
            'mobile',
            'android_studio',
            'android',
            'java',
            'kotlin',

            'fall-week-15-no-class'
          ]
        }
      ],
      '/spring/':[
        {
          title: '2021 Spring Lecture',   // required
          collapsable: false, // optional, defaults to true
          children: [
            'spring-week-1-no-class',

            'spring-week-2-no-class',

            'spring-week-3',

            'spring-week-4',
            'xcode',
            'swift',
            'object-c',

            'spring-week-5',
            'ios',

            'spring-week-6-no-class',
            'spring-week-7-no-class',
            'spring-week-8-no-class',
            'spring-week-9-no-class',
            'spring-week-10-no-class',
            'spring-week-11-no-class',
            'spring-week-12-no-class',
            'spring-week-13-no-class',

            'spring-week-14-no-class',
            'spring-week-15-no-class',
            'spring-week-16-no-class',

            'docusaurus',
            'crud',
            'search',
            'tag',
            'sync',
            'uuid',
            'tarpc',
            'upgrade',
            'pagination',
            'd3.js',
            'i18n',
            'web_hosting',
            'firebase',
            'app_store',
            'google_play',
            'chrome_web_store',
            'firefox_addons',
            'dapp',
            'ssb',
            'fastxt'
          ]
        }
      ],
      '/':[
        {
          // title: 'Introduction',   // required
          path: '',      // optional, which should be a absolute path.
          collapsable: false, // optional, defaults to true
          sidebarDepth: 1,    // optional, defaults to 1
          children: [
            '',
            'README-zh'
          ]
        },
        {
          // title: 'For Maker',   // required
          path: '',      // optional, which should be a absolute path.
          collapsable: false, // optional, defaults to true
          sidebarDepth: 1,    // optional, defaults to 1
          children: [
            ['/fall/fall-week-1', 'Fall Lecture'],
            ['/spring/spring-week-1-no-class', 'Spring Lecture'],
            ['/diagram/diagram-1', 'Diagram'],
            ['/fall/downloads', 'Downloads']
          ]
        },
        {
          // title: 'For Maker',   // required
          path: '',      // optional, which should be a absolute path.
          collapsable: false, // optional, defaults to true
          sidebarDepth: 1,    // optional, defaults to 1
          children: [
            'source-code',
            'license'
          ]
        }
      ]
    }
  }
}
